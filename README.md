#  SortableTableView

#### 项目介绍
- 项目名称：SortableTableView
- 所属系列：openharmony的第三方组件适配移植
- 功能：支持按列排序的列表组件
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 Beta1
- 基线版本：Release  2.8.1
  
#### 效果演示
![screen1](https://images.gitee.com/uploads/images/2021/0603/111230_bb17979b_8946212.gif "screen1.gif")
  
![screen2](https://images.gitee.com/uploads/images/2021/0603/111311_f1d2475f_8946212.gif "screen2.gif")
  
  
#### 安装教程
  
1.在项目根目录下的build.gradle文件中，
  
 ```
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}

 ```
  
2.在entry模块的build.gradle文件中，

 ```

 dependencies {
    implementation('com.gitee.chinasoft_ohos:tableview:1.0.0')
    ......
 }

 ```
在sdk6，DevEco Studio2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下
  
#### 使用说明
  
1.将仓库导入到本地仓库中

2.在布局文件中加入SortableCarTableView控件,代码实例如下:
```
      <com.de.codecrafters.tableviewexample.SortableCarTableView
          ohos:id="$+id:tableView"
          ohos:height="match_parent"
          ohos:width="match_parent"
          ohos:below="$id:toolbar"
          app:tableView_columnCount="4"
          app:tableView_headerColor="$color:primary"
          app:tableView_headerElevation="10"
          />
```
3.在AbilitySlice的onStart方法中获取控件,并设置数据,代码实例如下:

```
        carTableView = (SortableCarTableView) findComponentById(ResourceTable.Id_tableView);
        if (carTableView != null) {
            carTableDataAdapter = new CarTableDataAdapter(this, DataFactory.createCarList(), carTableView);
            carTableView.addDataClickListener(new CarClickListener());
            carTableView.addDataLongClickListener(new CarLongClickListener());
            this.getUITaskDispatcher().delayDispatch(() -> {
                carTableView.setDataAdapter(carTableDataAdapter);
            }, 100);
```
  
#### 测试信息
  
CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异
  
  
#### 版本迭代

- 1.0.0
- 0.0.1-SNAPSHOT
  
#### 版权和许可信息
```
*Copyright 2015 Ingo Schwarz*  
  
Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
```