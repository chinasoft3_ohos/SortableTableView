package de.codecrafters.tableview;

import de.codecrafters.tableview.listeners.TableHeaderClickListener;
import ohos.agp.components.Component;

import java.util.Set;


/**
 * A internal {@link Component.ClickedListener} on the header views that will forward clicks to the
 * registered {@link TableHeaderClickListener}.
 *
 * @author ISchwarz
 */
public class InternalHeaderClickListener implements Component.ClickedListener {

    private final Set<TableHeaderClickListener> listeners;
    private final int columnIndex;

    public InternalHeaderClickListener(final int columnIndex, final Set<TableHeaderClickListener> listeners) {
        this.columnIndex = columnIndex;
        this.listeners = listeners;
    }

    @Override
    public void onClick(final Component view) {
        informHeaderListeners();
    }

    private void informHeaderListeners() {
        for (final TableHeaderClickListener listener : listeners) {
            listener.onHeaderClicked(columnIndex);
        }
    }
}
