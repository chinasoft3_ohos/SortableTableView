package de.codecrafters.tableview;

import de.codecrafters.tableview.colorizers.TableDataRowColorizer;
import de.codecrafters.tableview.providers.TableDataRowBackgroundProvider;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;


/**
 * A wrapper for a {@link TableDataRowColorizer} that implements the {@link TableDataRowBackgroundProvider} interface.
 *
 * @author ISchwarz
 * @deprecated The {@link TableDataRowBackgroundColorProvider} is deprecated as it is only used to map a
 * {@link TableDataRowColorizer} to a {@link TableDataRowBackgroundProvider}.
 */
@Deprecated
class TableDataRowBackgroundColorProvider<T> implements TableDataRowBackgroundProvider<T> {

    private final TableDataRowColorizer<T> colorizer;

    /**
     * Creates a new {@link TableDataRowBackgroundColorProvider} using the given {@link TableDataRowColorizer}.
     *
     * @param colorizer The {@link TableDataRowColorizer} that shall be wrapped.
     */
    public TableDataRowBackgroundColorProvider(final TableDataRowColorizer<T> colorizer) {
        this.colorizer = colorizer;
    }

    @Override
    public Element getRowBackground(int rowIndex, Object rowData) {
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(RgbColor.fromArgbInt(colorizer.getRowColor(rowIndex, (T) rowData)));
        return shapeElement;
    }
}
