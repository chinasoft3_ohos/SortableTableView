package de.codecrafters.tableview;

/**
 * 排序方式
 */
public enum SortingOrder {
    ASCENDING,
    DESCENDING
}
