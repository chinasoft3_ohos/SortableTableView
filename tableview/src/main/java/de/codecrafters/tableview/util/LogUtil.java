/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.codecrafters.tableview.util;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * LogUtil
 */
public class LogUtil {
    /**
     * Log TAG
     */
    public static final String TAG = "HMS_TAG";
    private static final HiLogLabel LABEL = new HiLogLabel(ohos.hiviewdfx.HiLog.LOG_APP, 0x00201, TAG);
    /**
     * Log FLAG
     */
    public static final boolean FLAG = false;

    /**
     * 打印debug级别日志
     *
     * @param msg 日志
     */
    public static void debug(String msg) {
        if (!FLAG) {
            HiLog.debug(LABEL, msg);
        }
    }

    /**
     * 打印error级别日志
     *
     * @param msg 日志
     */
    public static void error(String msg) {
        if (!FLAG) {
            HiLog.error(LABEL, msg);
        }
    }

    /**
     * 打印warn级别日志
     *
     * @param msg 日志
     */
    public static void warn(String msg) {
        if (!FLAG) {
            HiLog.warn(LABEL, msg);

        }
    }

    /**
     * 打印info级别日志
     *
     * @param msg 日志
     */
    public static void info(String msg) {
        if (!FLAG) {
            HiLog.info(LABEL, msg);
        }
    }

    /**
     * 打印fatal级别日志
     *
     * @param msg 日志
     */
    public static void fatal(String msg) {
        if (!FLAG) {
            HiLog.fatal(LABEL, msg);
        }
    }
}
