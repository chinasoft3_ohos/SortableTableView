package de.codecrafters.tableview.toolkit;

import de.codecrafters.tableview.TableDataAdapter;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Text;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;

import java.util.Arrays;
import java.util.List;

/**
 * A simple {@link TableDataAdapter} that allows to display 2D-String-Arrays in a {@link de.codecrafters.tableview.TableView}.
 *
 * @author ISchwarz
 */
public final class SimpleTableDataAdapter extends TableDataAdapter<String[]> {

    private static final String LOG_TAG = SimpleTableDataAdapter.class.getName();

    private int paddingLeft = 20;
    private int paddingTop = 15;
    private int paddingRight = 20;
    private int paddingBottom = 15;
    private int textSize = 18;
    private Font typeface = Font.DEFAULT;
    private int textColor = 0x99000000;
    private int gravity = TextAlignment.START;
    private List<String[]> data;


    public SimpleTableDataAdapter(final Context context, final String[][] data) {
        super(context, data);
        List<String[]> dataList = Arrays.asList(data);
        this.data = dataList;
    }

    public SimpleTableDataAdapter(final Context context, final List<String[]> data) {
        super(context, data);
        this.data = data;
    }

    @Override
    public Component getCellView(int rowIndex, int columnIndex, ComponentContainer parentView) {
        final Text textView = new Text(getContext());
        textView.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
        textView.setFont(typeface);
        textView.setTextSize(textSize);
        Color color = new Color(textColor);
        textView.setTextColor(color);
        textView.setMaxTextLines(1);
        textView.setTruncationMode(Text.TruncationMode.ELLIPSIS_AT_END);

        final String textToShow = getItem(rowIndex)[columnIndex];
        textView.setText(textToShow);
        textView.setTextAlignment(gravity);

        return textView;
    }


    /**
     * Sets the gravity of the text inside the data cell.
     *
     * @param gravity Sets the gravity of the text inside the data cell.
     */
    public void setGravity(int gravity) {
        this.gravity = gravity;
    }

    /**
     * Sets the padding that will be used for all table cells.
     *
     * @param left   The padding on the left side.
     * @param top    The padding on the top side.
     * @param right  The padding on the right side.
     * @param bottom The padding on the bottom side.
     */
    public void setPaddings(final int left, final int top, final int right, final int bottom) {
        paddingLeft = left;
        paddingTop = top;
        paddingRight = right;
        paddingBottom = bottom;
    }

    /**
     * Sets the padding that will be used on the left side for all table cells.
     *
     * @param paddingLeft The padding on the left side.
     */
    public void setPaddingLeft(final int paddingLeft) {
        this.paddingLeft = paddingLeft;
    }

    /**
     * Sets the padding that will be used on the top side for all table cells.
     *
     * @param paddingTop The padding on the top side.
     */
    public void setPaddingTop(final int paddingTop) {
        this.paddingTop = paddingTop;
    }

    /**
     * Sets the padding that will be used on the right side for all table cells.
     *
     * @param paddingRight The padding on the right side.
     */
    public void setPaddingRight(final int paddingRight) {
        this.paddingRight = paddingRight;
    }

    /**
     * Sets the padding that will be used on the bottom side for all table cells.
     *
     * @param paddingBottom The padding on the bottom side.
     */
    public void setPaddingBottom(final int paddingBottom) {
        this.paddingBottom = paddingBottom;
    }

    /**
     * Sets the text size that will be used for all table cells.
     *
     * @param textSize The text size that shall be used.
     */
    public void setTextSize(final int textSize) {
        this.textSize = textSize;
    }

    /**
     * Sets the typeface that will be used for all table cells.
     *
     * @param typeface The type face that shall be used.
     */
    public void setTypeface(final Font typeface) {
        this.typeface = typeface;
    }

    /**
     * Sets the text color that will be used for all table cells.
     *
     * @param textColor The text color that shall be used.
     */
    public void setTextColor(final int textColor) {
        this.textColor = textColor;
    }


    @Override
    public int getCount() {
        if (null == data || data.size() <= 0) {
            return 0;
        } else {
            return data.size();
        }
    }

    @Override
    public String[] getItem(int i) {
        return data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }
}
