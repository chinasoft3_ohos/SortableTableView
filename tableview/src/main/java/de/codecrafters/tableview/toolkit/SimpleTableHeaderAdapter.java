package de.codecrafters.tableview.toolkit;

import de.codecrafters.tableview.TableHeaderAdapter;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Text;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;


/**
 * Simple implementation of the {@link TableHeaderAdapter}. This adapter will render the given header
 * Strings as {@link ohos.agp.components.Text}.
 *
 * @author ISchwarz
 */
public final class SimpleTableHeaderAdapter extends TableHeaderAdapter {

    private final String[] headers;
    private int paddingLeft = 20;
    private int paddingTop = 30;
    private int paddingRight = 20;
    private int paddingBottom = 30;
    private int textSize = 18;
    private int textColor = 0x99000000;
    private Font typeface = new Font.Builder("BOLD").setWeight(Font.BOLD).build();
    private int gravity = TextAlignment.START;

    /**
     * Creates a new SimpleTableHeaderAdapter.
     *
     * @param context The context to use inside this {@link TableHeaderAdapter}.
     * @param headers The header labels that shall be rendered.
     */
    public SimpleTableHeaderAdapter(final Context context, final String... headers) {
        super(context);
        this.headers = headers;
    }

    public SimpleTableHeaderAdapter(final Context context, final int... headerStringResources) throws NotExistException, WrongTypeException, IOException {
        super(context);
        this.headers = new String[headerStringResources.length];

        for (int i = 0; i < headerStringResources.length; i++) {
            headers[i] = context.getResourceManager().getElement(headerStringResources[i]).getString();
        }
    }

    /**
     * Sets the padding that will be used for all table headers.
     *
     * @param left   The padding on the left side.
     * @param top    The padding on the top side.
     * @param right  The padding on the right side.
     * @param bottom The padding on the bottom side.
     */
    public void setPaddings(final int left, final int top, final int right, final int bottom) {
        paddingLeft = left;
        paddingTop = top;
        paddingRight = right;
        paddingBottom = bottom;
    }

    /**
     * Sets the gravity of the text inside the header cell.
     *
     * @param gravity The gravity of the text inside the header cell.
     */
    public void setGravity(int gravity) {
        this.gravity = gravity;
    }

    /**
     * Sets the padding that will be used on the left side for all table headers.
     *
     * @param paddingLeft The padding on the left side.
     */
    public void setPaddingLeft(final int paddingLeft) {
        this.paddingLeft = paddingLeft;
    }

    /**
     * Sets the padding that will be used on the top side for all table headers.
     *
     * @param paddingTop The padding on the top side.
     */
    public void setPaddingTop(final int paddingTop) {
        this.paddingTop = paddingTop;
    }

    /**
     * Sets the padding that will be used on the right side for all table headers.
     *
     * @param paddingRight The padding on the right side.
     */
    public void setPaddingRight(final int paddingRight) {
        this.paddingRight = paddingRight;
    }

    /**
     * Sets the padding that will be used on the bottom side for all table headers.
     *
     * @param paddingBottom The padding on the bottom side.
     */
    public void setPaddingBottom(final int paddingBottom) {
        this.paddingBottom = paddingBottom;
    }

    /**
     * Sets the text size that will be used for all table headers.
     *
     * @param textSize The text size that shall be used.
     */
    public void setTextSize(final int textSize) {
        this.textSize = textSize;
    }

    /**
     * Sets the typeface that will be used for all table headers.
     *
     * @param typeface The type face that shall be used.
     */
    public void setTypeface(final Font typeface) {
        this.typeface = typeface;
    }

    /**
     * Sets the text color that will be used for all table headers.
     *
     * @param textColor The text color that shall be used.
     */
    public void setTextColor(final int textColor) {
        this.textColor = textColor;
    }

    @Override
    public Component getHeaderView(int columnIndex, ComponentContainer parentView) {
        final Text textView = new Text(getContext());

        if (columnIndex < headers.length) {
            textView.setText(headers[columnIndex]);
            textView.setTextAlignment(gravity);
        }

        textView.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
        textView.setFont(typeface);
        textView.setTextSize(textSize, Text.TextSizeType.VP);
        Color color = new Color(textColor);
        textView.setTextColor(color);
        textView.setMaxTextLines(1);
        textView.setTruncationMode(Text.TruncationMode.ELLIPSIS_AT_END);

        return textView;
    }

    @Override
    public Object getItem(int i) {
        return headers[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }
}
