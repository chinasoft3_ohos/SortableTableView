/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.codecrafters.tableview.swiperefreshlayout;

import de.codecrafters.tableview.ResourceTable;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.*;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.multimodalinput.event.TouchEvent;

/**
 * 下拉刷新
 */
public class SwiperefreshLayout extends DependentLayout implements Component.TouchEventListener {

    private SwiperefreshLoadMoreView mRecycleView;
    private Image mIvRefresh;
    private AnimatorProperty mRefreshAnimator;
    private VelocityDetector mVelocityTracker;
    // 记录上一次手指的Y坐标
    private double mLastY = 0;
    private ListContainer mListContainer;
    private boolean isRefresh;
    private OnPullRefreshListener mOnPullRefreshListener;

    public SwiperefreshLayout(Context context) {
        this(context, null);
    }

    public SwiperefreshLayout(Context context, AttrSet attrSet) {
        this(context, attrSet, "");
    }

    public SwiperefreshLayout(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        setContent(context);
    }

    private void setContent(Context context) {
        LayoutScatter.getInstance(context).parse(ResourceTable.Layout_layout_swiperefreshlayout, this, true);
        mRecycleView = (SwiperefreshLoadMoreView) findComponentById(ResourceTable.Id_recycle_view);
        mIvRefresh = (Image) findComponentById(ResourceTable.Id_iv_refresh);
        mRecycleView.setTouchEventListener(this::onTouchEvent);
        mRefreshAnimator = mIvRefresh.createAnimatorProperty();

    }

    // 开始下拉刷新动画
    private void startRefreshAnimator() {
        mIvRefresh.setVisibility(Component.VISIBLE);
        mRefreshAnimator.setDuration(1000).rotate(360).setLoopedCount(AnimatorProperty.INFINITE).setTarget(mIvRefresh).start();
    }

    /**
     * 关闭下拉刷新动画
     */
    public void stopRefreshAnimator() {
        mIvRefresh.setVisibility(Component.HIDE);
        mRefreshAnimator.cancel();
    }


    /**
     * 添加contentView
     *
     * @param contentView contentView
     */
    public void addContentView(Component contentView) {
        if (contentView != null) {
            if (contentView instanceof ListContainer) {
                mListContainer = (ListContainer) contentView;
                mRecycleView.addContentView(mListContainer);
            }

        }
    }

    /**
     * 删除contentView
     *
     * @param contentView contentView
     */
    public void removeContentView(Component contentView) {
        if (contentView != null) {
            mRecycleView.removeContentView(contentView);
        }
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent ev) {
        if (mVelocityTracker == null) {
            mVelocityTracker = VelocityDetector.obtainInstance();
        }
        mVelocityTracker.addEvent(ev);
        return setAction(ev);

    }

    // 设置是否能够下拉刷新
    private boolean setAction(TouchEvent ev) {
        final int action = ev.getAction();
        int sumY = 0;
        switch (action) {
            // 单指触摸
            case TouchEvent.PRIMARY_POINT_DOWN:
                mLastY = getPostionY(ev, ev.getIndex());
                break;
            case TouchEvent.POINT_MOVE:
                // 下拉  offsetY 都>0,上拉  第一次offsetY>0,第二次offsetY<0;
                double currentY = getPostionY(ev, ev.getIndex());
                double scrollY = currentY - mLastY;
                int offsetY = (int) scrollY;
                if ((offsetY > 0) && (!mListContainer.canScroll(10)) && (!isRefresh)) {
                    if (mListContainer.getItemPosByVisibleIndex(0) != 0) {
                        return false;
                    } else {
                        refresh();
                    }
                }
                mLastY = currentY;
                break;

            case TouchEvent.PRIMARY_POINT_UP:
                endRefreshDelay();
                break;
            case TouchEvent.CANCEL:
                moveOver();
                break;
        }
        return true;
    }

    private void endRefreshDelay() {
        new EventHandler(EventRunner.getMainEventRunner()).postTask(new Runnable() {
            @Override
            public void run() {
                moveOver();
            }
        }, 1000);
    }

    private void refresh() {
        isRefresh = true;
        startRefreshAnimator();
        if (mOnPullRefreshListener != null) {
            mOnPullRefreshListener.onPullRefresh();
        }
    }

    private float getPostionY(TouchEvent ev, int index) {
        return ev.getPointerPosition(index).getY();
    }

    private void moveOver() {
        stopRefreshAnimator();
        isRefresh = false;
    }

    /**
     * 设置下拉刷新监听
     *
     * @param onPullRefreshListener 刷新接口
     */
    public void setOnPullRefreshListener(OnPullRefreshListener onPullRefreshListener) {
        this.mOnPullRefreshListener = onPullRefreshListener;
    }

    /**
     * 下拉刷新接口
     */
    public interface OnPullRefreshListener {
        void onPullRefresh();
    }
}
