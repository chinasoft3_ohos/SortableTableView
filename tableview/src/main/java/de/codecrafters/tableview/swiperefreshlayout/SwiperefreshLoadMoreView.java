/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.codecrafters.tableview.swiperefreshlayout;

import de.codecrafters.tableview.ResourceTable;
import ohos.agp.components.*;
import ohos.app.Context;

/**
 * 加载更多
 */
public class SwiperefreshLoadMoreView extends DirectionalLayout {

    private DependentLayout mContentView;
    private Component mComponent;

    public SwiperefreshLoadMoreView(Context context) {
        this(context, null);
    }

    public SwiperefreshLoadMoreView(Context context, AttrSet attrSet) {
        this(context, attrSet, "");
    }

    public SwiperefreshLoadMoreView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        setContent(context);
    }

    private void setContent(Context context) {
        LayoutScatter.getInstance(context).parse(ResourceTable.Layout_layout_load_more, this, true);
        mContentView = (DependentLayout) findComponentById(ResourceTable.Id_list_layout);
    }

    /**
     * addContentView
     *
     * @param contentView contentView
     */
    public void addContentView(Component contentView) {
        if (contentView != null) {
            mComponent = contentView;
            mContentView.addComponent(mComponent);
        }
    }

    /**
     * removeContentView
     *
     * @param contentView contentView
     */
    public void removeContentView(Component contentView) {
        if (contentView != null) {
            mContentView.removeComponent(contentView);
        }
    }

    /**
     * getContentView
     *
     * @return Component
     */
    public Component getContentView() {
        return mComponent;
    }
}
