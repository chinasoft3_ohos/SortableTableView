/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.codecrafters.tableview;

/**
 * 自定义控件自定义属性名称配置
 *
 * @author Longaike
 */
public class AttrSetString {

    /**
     * 头部颜色
     */
    public static final String TABLEVIEW_HEADERCOLOR = "tableView_headerColor";

    /**
     * 阴影值
     */
    public static final String TABLEVIEW_HEADERELEVATION = "tableView_headerElevation";

    /**
     * 列数
     */
    public static final String TABLEVIEW_COLUMNCOUNT = "tableView_columnCount";

}
