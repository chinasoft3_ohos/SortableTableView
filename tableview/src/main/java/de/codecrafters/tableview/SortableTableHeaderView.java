package de.codecrafters.tableview;

import de.codecrafters.tableview.model.TableColumnModel;
import de.codecrafters.tableview.providers.SortStateViewProvider;
import de.codecrafters.tableview.toolkit.SortStateViewProviders;
import ohos.agp.components.*;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.ResourceManager;
import ohos.global.resource.WrongTypeException;
import ohos.utils.PlainArray;

import java.io.IOException;
import java.util.Optional;


/**
 * Extension of the {@link TableHeaderView} that will show sorting indicators at the start of the header.
 *
 * @author ISchwarz
 */
class SortableTableHeaderView extends TableHeaderView {

    private final PlainArray<Image> sortViews = new PlainArray<>();
    private final PlainArray<SortState> sortStates = new PlainArray<>();

    private SortStateViewProvider sortStateViewProvider = SortStateViewProviders.darkArrows();


    /**
     * Creates a new SortableTableHeaderView.
     *
     * @param context The context that shall be used.
     */
    public SortableTableHeaderView(final Context context) {
        super(context);
    }

    /**
     * Will set all sort views to state "sortable".
     */
    public void resetSortViews() {
        for (int i = 0; i < sortStates.size(); i++) {
            final int columnIndex = sortStates.keyAt(i);

            Optional<SortState> columnSortState = sortStates.get(columnIndex);
            if (columnSortState.isPresent()&&columnSortState.get() != SortState.NOT_SORTABLE) {
                sortStates.setValueAt(columnIndex, SortState.SORTABLE);
            }
            sortStates.put(columnIndex, sortStates.get(columnIndex).get());
        }

        for (int i = 0; i < sortStates.size(); i++) {
            final int columnIndex = sortStates.keyAt(i);
            final Optional<Image> sortView = sortViews.get(columnIndex);
            final Optional<SortState> sortState = sortStates.get(columnIndex);
            if (sortState.isPresent()&&sortView.isPresent()){
                setSortStateToView(sortState.get(), sortView.get());
            }
        }
    }

    @Override
    public void setAdapter(TableHeaderAdapter adapter) {
        super.setAdapter(new SortStateArrayAdapter(adapter));
    }

    /**
     * Sets the {@link SortState} of the SortView of the column with the given index.
     *
     * @param columnIndex The index of the column for which the given {@link SortState}
     *                    will be set.
     * @param sortState   The {@link SortState} that shall be set to the sort view at the column with
     *                    the given index.
     */
    public void setSortState(final int columnIndex, final SortState sortState) {
        sortStates.put(columnIndex, sortState);
        invalidate();
    }

    private void setSortStateToView(final SortState state, final Image view) {

        if (view != null) {
            final int imageRes = sortStateViewProvider.getSortStateViewResource(state);
            view.setScaleMode(Image.ScaleMode.INSIDE);
            view.setPixelMap(imageRes);
            if (imageRes == 0) {
                view.setVisibility(HIDE);
            } else {
                view.setVisibility(VISIBLE);
            }
        }
    }

    /**
     * Gives the current {@link SortStateViewProvider} of this SortableTableHeaderView.
     *
     * @return The {@link SortStateViewProvider} that is currently used to render the sort views.
     */
    public SortStateViewProvider getSortStateViewProvider() {
        return sortStateViewProvider;
    }

    /**
     * Sets the given {@link SortStateViewProvider} to this SortableTableHeaderView.
     *
     * @param provider The {@link SortStateViewProvider} that shall be used to render the sort views.
     */
    public void setSortStateViewProvider(final SortStateViewProvider provider) {
        sortStateViewProvider = provider;
        invalidate();
    }

    @Override
    public void invalidate() {
        if (getAdapter() != null) {
            getAdapter().notifyDataChanged();
        }
        super.invalidate();
    }

    public TableHeaderAdapter getAdapter() {

        if (super.getItemProvider() instanceof SortStateArrayAdapter) {
            return ((SortStateArrayAdapter) super.getItemProvider()).delegate;
        }
        return (TableHeaderAdapter) super.getItemProvider();
    }

    private class SortStateArrayAdapter extends TableHeaderAdapter {

        private final TableHeaderAdapter delegate;

        public SortStateArrayAdapter(final TableHeaderAdapter delegate) {
            super(delegate.getContext());
            this.delegate = delegate;
        }

        @Override
        public Context getContext() {
            return delegate.getContext();
        }

        @Override
        public LayoutScatter getLayoutInflater() {
            return delegate.getLayoutInflater();
        }

        @Override
        public ResourceManager getResources() {
            return delegate.getResources();
        }

        @Override
        public TableColumnModel getColumnModel() {
            return delegate.getColumnModel();
        }

        @Override
        public void setColumnModel(TableColumnModel columnModel) {
            delegate.setColumnModel(columnModel);
        }

        @Override
        public int getColumnCount() {
            return delegate.getColumnCount();
        }

        @Override
        public void setColumnCount(int columnCount) {
            delegate.setColumnCount(columnCount);
        }

        @Override
        public int getCount() {
            return delegate.getCount();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public Component getHeaderView(final int columnIndex, final ComponentContainer parentView) throws NotExistException, WrongTypeException, IOException {

            // create column header layout
            final DirectionalLayout headerLayout = (DirectionalLayout) delegate.getLayoutInflater().parse(ResourceTable.Layout_sortable_header, parentView, false);
            headerLayout.setClickedListener(new InternalHeaderClickListener(columnIndex, getHeaderClickListeners()));

            // create header
            Component headerView = delegate.getHeaderView(columnIndex, headerLayout);
            if (headerView == null) {
                headerView = new Text(getContext());
            }

            // add the header view to the header layout
            ((StackLayout) headerLayout.findComponentById(ResourceTable.Id_container)).addComponent(headerView);

            // get the sort view
            Image sortView = (Image) headerLayout.findComponentById(ResourceTable.Id_sort_view);
            sortViews.put(columnIndex, sortView);

            // get the sort state image
            Optional<SortState> sortState = sortStates.get(columnIndex);
            if (!sortState.isPresent()) {
                sortStates.setValueAt(columnIndex, SortState.NOT_SORTABLE);
                sortStates.put(columnIndex, sortStates.get(columnIndex).get());
            }else {
                setSortStateToView(sortState.get(), sortView);
            }

            return headerLayout;
        }
    }

}
