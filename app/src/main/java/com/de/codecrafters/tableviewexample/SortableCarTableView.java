package com.de.codecrafters.tableviewexample;


import com.de.codecrafters.tableviewexample.data.Car;
import de.codecrafters.tableview.SortableTableView;
import de.codecrafters.tableview.model.TableColumnWeightModel;
import de.codecrafters.tableview.toolkit.SimpleTableHeaderAdapter;
import de.codecrafters.tableview.toolkit.SortStateViewProviders;
import de.codecrafters.tableview.toolkit.TableDataRowBackgroundProviders;
import de.codecrafters.tableview.util.LogUtil;
import ohos.agp.components.AttrSet;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;

/**
 * An extension of the {@link SortableTableView} that handles {@link Car}s.
 *
 * @author ISchwarz
 */
public class SortableCarTableView extends SortableTableView<Car> {

    public SortableCarTableView(final Context context) {
        super(context);
        init(context);
    }


    public SortableCarTableView(final Context context, final AttrSet attributes) {
        super(context, attributes);
        init(context);
    }

    public SortableCarTableView(final Context context, final AttrSet attributes, final int styleAttributes) {
        super(context, attributes, styleAttributes);
        init(context);
    }

    private void init(Context context) {
        SimpleTableHeaderAdapter simpleTableHeaderAdapter = null;
        try {
            simpleTableHeaderAdapter = new SimpleTableHeaderAdapter(context, ResourceTable.String_brand, ResourceTable.String_model, ResourceTable.String_power, ResourceTable.String_price);
            simpleTableHeaderAdapter.setTextColor(context.getResourceManager().getElement(ResourceTable.Color_table_header_text).getColor());
            setHeaderAdapter(simpleTableHeaderAdapter);
        } catch (IOException e) {
            LogUtil.error(e.getMessage());
        } catch (NotExistException e) {
            LogUtil.error(e.getMessage());
        } catch (WrongTypeException e) {
            LogUtil.error(e.getMessage());
        }


        int rowColorEven = 0;
        int rowColorOdd = 0;
        try {
            rowColorEven = context.getResourceManager().getElement(ResourceTable.Color_table_data_row_even).getColor();
            rowColorOdd = context.getResourceManager().getElement(ResourceTable.Color_table_data_row_odd).getColor();
        } catch (IOException e) {
            LogUtil.error(e.getMessage());
        } catch (NotExistException e) {
            LogUtil.error(e.getMessage());
        } catch (WrongTypeException e) {
            LogUtil.error(e.getMessage());
        }

        setDataRowBackgroundProvider(TableDataRowBackgroundProviders.alternatingRowColors(rowColorEven, rowColorOdd));
        setHeaderSortStateViewProvider(SortStateViewProviders.brightArrows());

        final TableColumnWeightModel tableColumnWeightModel = new TableColumnWeightModel(4);
        tableColumnWeightModel.setColumnWeight(0, 2);
        tableColumnWeightModel.setColumnWeight(1, 3);
        tableColumnWeightModel.setColumnWeight(2, 3);
        tableColumnWeightModel.setColumnWeight(3, 2);
        setColumnModel(tableColumnWeightModel);

        setColumnComparator(0, CarComparators.getCarProducerComparator());
        setColumnComparator(1, CarComparators.getCarNameComparator());
        setColumnComparator(2, CarComparators.getCarPowerComparator());
        setColumnComparator(3, CarComparators.getCarPriceComparator());
    }
}
