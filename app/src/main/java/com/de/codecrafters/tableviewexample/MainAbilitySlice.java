package com.de.codecrafters.tableviewexample;

import com.de.codecrafters.tableviewexample.data.Car;
import com.de.codecrafters.tableviewexample.data.DataFactory;
import de.codecrafters.tableview.listeners.TableDataClickListener;
import de.codecrafters.tableview.listeners.TableDataLongClickListener;
import de.codecrafters.tableview.swiperefreshlayout.SwiperefreshLayout;
import de.codecrafters.tableview.util.LogUtil;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;
import java.security.SecureRandom;
import java.util.List;

public class MainAbilitySlice extends AbilitySlice {
    private Context context;
    private CarTableDataAdapter carTableDataAdapter;
    private SortableCarTableView carTableView;

    @Override
    public void onStart(Intent intent) {
        try {
            int color = getResourceManager().getElement(ResourceTable.Color_primaryDark).getColor();
            getWindow().setStatusBarColor(color);
        } catch (IOException | NotExistException | WrongTypeException e) {
            e.printStackTrace();
        }
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        context = this;
        carTableView = (SortableCarTableView) findComponentById(ResourceTable.Id_tableView);

        if (carTableView != null) {
            carTableDataAdapter = new CarTableDataAdapter(this, DataFactory.createCarList(), carTableView);
            carTableView.addDataClickListener(new CarClickListener());
            carTableView.addDataLongClickListener(new CarLongClickListener());
            this.getUITaskDispatcher().delayDispatch(() -> {
                carTableView.setDataAdapter(carTableDataAdapter);
            }, 100);
            carTableView.setOnPullRefreshListener(new SwiperefreshLayout.OnPullRefreshListener() {
                @Override
                public void onPullRefresh() {
                    LogUtil.error("MainAbilitySlice" + ":  " + "setOnPullRefreshListener");
                    context.getUITaskDispatcher().delayDispatch(() -> {
                        final Car randomCar = getRandomCar();
                        carTableDataAdapter.getData().add(randomCar);
                        carTableDataAdapter.notifyDataChanged();
                        showToast("Added: " + randomCar);
                        carTableView.stopRefresh();
                        LogUtil.error("MainAbilitySlice" + ":  " + "RefreshOver");
                    }, 1000);
                }
            });
        }
    }

    private Car getRandomCar() {
        final List<Car> carList = DataFactory.createCarList();
        final int randomCarIndex = Math.abs(new Random().getRandom() % carList.size());
        return carList.get(randomCarIndex);
    }


    static class Random {
        private SecureRandom ran;

        Random() {
            ran = new SecureRandom();
        }

        public int getRandom() {
            return ran.nextInt();
        }
    }

    private class CarClickListener implements TableDataClickListener<Car> {

        @Override
        public void onDataClicked(final int rowIndex, final Car clickedData) {
            final String carString = "Click: " + clickedData.getProducer().getName() + " " + clickedData.getName();
            showToast(carString);
        }
    }

    private class CarLongClickListener implements TableDataLongClickListener<Car> {

        @Override
        public boolean onDataLongClicked(final int rowIndex, final Car clickedData) {
            final String carString = "Long Click: " + clickedData.getProducer().getName() + " " + clickedData.getName();
            showToast(carString);
            return true;
        }
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    private void showToast(String text) {
        final ToastDialog toastDialog = new ToastDialog(MainAbilitySlice.this);
        toastDialog.setText(text);
        toastDialog.setAlignment(LayoutAlignment.BOTTOM);
        toastDialog.setOffset(0, 170);
        toastDialog.show();
    }
}
