package com.de.codecrafters.tableviewexample;


import com.de.codecrafters.tableviewexample.data.Car;

import java.io.Serializable;
import java.util.Comparator;


/**
 * A collection of {@link Comparator}s for {@link Car} objects.
 *
 * @author ISchwarz
 */
public final class CarComparators {

    private CarComparators() {
        // no instance
    }

    /**
     * getCarProducerComparator
     *
     * @return Comparator<Car>
     */
    public static Comparator<Car> getCarProducerComparator() {
        return new CarProducerComparator();
    }

    /**
     * getCarPowerComparator
     *
     * @return Comparator<Car>
     */
    public static Comparator<Car> getCarPowerComparator() {
        return new CarPowerComparator();
    }

    /**
     * getCarNameComparator
     *
     * @return Comparator<Car>
     */
    public static Comparator<Car> getCarNameComparator() {
        return new CarNameComparator();
    }

    /**
     * getCarPriceComparator
     *
     * @return Comparator<Car>
     */
    public static Comparator<Car> getCarPriceComparator() {
        return new CarPriceComparator();
    }

    private static class CarProducerComparator implements Comparator<Car>, Serializable {

        @Override
        public int compare(final Car car1, final Car car2) {
            return car1.getProducer().getName().compareTo(car2.getProducer().getName());
        }

    }

    private static class CarPowerComparator implements Comparator<Car>, Serializable {

        @Override
        public int compare(final Car car1, final Car car2) {
            return car1.getPs() - car2.getPs();
        }
    }

    private static class CarNameComparator implements Comparator<Car>, Serializable {

        @Override
        public int compare(final Car car1, final Car car2) {
            return car1.getName().compareTo(car2.getName());
        }
    }

    private static class CarPriceComparator implements Comparator<Car>, Serializable {

        @Override
        public int compare(final Car car1, final Car car2) {
            if (car1.getPrice() < car2.getPrice()) {
                return -1;
            } else if (car1.getPrice() > car2.getPrice()) {
                return 1;
            } else {
                return 0;
            }
        }
    }

}
