package com.de.codecrafters.tableviewexample;


import com.de.codecrafters.tableviewexample.data.Car;
import de.codecrafters.tableview.TableView;
import de.codecrafters.tableview.toolkit.LongPressAwareTableDataAdapter;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.List;

import static java.lang.String.format;


public class CarTableDataAdapter extends LongPressAwareTableDataAdapter<Car> {

    private static final int TEXT_SIZE = 12;
    private static final NumberFormat PRICE_FORMATTER = NumberFormat.getNumberInstance();
    List<Car> data;


    public CarTableDataAdapter(final Context context, final List<Car> data, final TableView<Car> tableView) {
        super(context, data, tableView);
        this.data = data;
    }

    @Override
    public Component getDefaultCellView(int rowIndex, int columnIndex, ComponentContainer parentView) throws IOException, NotExistException {
        final Car car = getRowData(rowIndex);
        Component renderedView = null;

        switch (columnIndex) {
            case 0:
                renderedView = renderProducerLogo(car, parentView);
                break;
            case 1:
                renderedView = renderCatName(car);
                break;
            case 2:
                renderedView = renderPower(car, parentView);
                break;
            case 3:
                renderedView = renderPrice(car);
                break;
        }
        return renderedView;
    }

    @Override
    public Component getLongPressCellView(int rowIndex, int columnIndex, ComponentContainer parentView) throws IOException, NotExistException {
        final Car car = getRowData(rowIndex);
        Component renderedView = null;

        switch (columnIndex) {
            case 1:
                renderedView = renderEditableCatName(car);
                break;
            default:
                renderedView = getDefaultCellView(rowIndex, columnIndex, parentView);
        }
        return renderedView;
    }

    private Component renderEditableCatName(final Car car) {
        final DirectionalLayout directionalLayout = new DirectionalLayout(getContext());
        directionalLayout.setOrientation(DirectionalLayout.VERTICAL);
        directionalLayout.setLayoutConfig(new DirectionalLayout.LayoutConfig(DirectionalLayout.LayoutConfig.MATCH_CONTENT, DirectionalLayout.LayoutConfig.MATCH_PARENT));
        Component line;

        final TextField editText = new TextField(getContext());
        editText.setLayoutConfig(new DirectionalLayout.LayoutConfig(DirectionalLayout.LayoutConfig.MATCH_PARENT, DirectionalLayout.LayoutConfig.MATCH_PARENT));
        editText.setText(car.getName());
        editText.setMarginsLeftAndRight(10, 10);
        editText.setPadding(20, 10, 10, 10);
        editText.setTextSize(TEXT_SIZE, Text.TextSizeType.VP);
        editText.setFont(Font.SERIF);
        editText.setFont(Font.DEFAULT_BOLD);
        editText.setMultipleLine(false);
        editText.setBubbleHeight(0);
        editText.setBubbleWidth(0);
        editText.addTextObserver(new CarNameUpdater(car));
        editText.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                final ShapeElement shapeElement = new ShapeElement();
                shapeElement.setRgbColor(RgbColor.fromArgbInt(Color.getIntColor("#3CB371")));
                final DirectionalLayout parent = (DirectionalLayout) component.getComponentParent();
                Component line = parent.findComponentById(122);
                line.setHeight(4);
                line.setBackground(shapeElement);
            }
        });
        directionalLayout.addComponent(editText);

        line = new Component(getContext());
        line.setWidth(DirectionalLayout.LayoutConfig.MATCH_PARENT);
        line.setHeight(4);
        line.setId(122);
        final ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(RgbColor.fromArgbInt(Color.getIntColor("#000000")));
        line.setBackground(shapeElement);
        directionalLayout.addComponent(line);
        return directionalLayout;
    }

    private Component renderPrice(final Car car) {
        final String priceString = PRICE_FORMATTER.format(car.getPrice()) + " €";

        final Text textView = new Text(getContext());
        textView.setText(priceString);
        textView.setPadding(20, 10, 10, 10);
        textView.setTextSize(TEXT_SIZE, Text.TextSizeType.VP);

        if (car.getPrice() < 50000) {
            textView.setTextColor(new Color(Color.getIntColor("#FF2E7D32")));
        } else if (car.getPrice() > 100000) {
            textView.setTextColor(new Color(Color.getIntColor("#FFC62828")));
        }

        return textView;
    }

    private Component renderPower(final Car car, final ComponentContainer parentView) throws IOException, NotExistException {
        final Component view = getLayoutInflater().parse(ResourceTable.Layout_table_cell_power, parentView, false);
        final Text kwView = (Text) view.findComponentById(ResourceTable.Id_kw_view);
        final Text psView = (Text) view.findComponentById(ResourceTable.Id_ps_view);

        try {
            kwView.setText(format("%d %s", car.getKw(), getContext().getResourceManager().getElement(ResourceTable.String_kw).getString()));
            psView.setText(format("%d %s", car.getPs(), getContext().getResourceManager().getElement(ResourceTable.String_ps).getString()));
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
        return view;
    }

    private Component renderCatName(final Car car) {
        return renderString(car.getName());
    }

    private Component renderProducerLogo(final Car car, final ComponentContainer parentView) {
        final Component view = getLayoutInflater().parse(ResourceTable.Layout_table_cell_image, parentView, false);
        final Image imageView = (Image) view.findComponentById(ResourceTable.Id_imageView);
        imageView.setScaleMode(Image.ScaleMode.INSIDE);
        imageView.setPixelMap(car.getProducer().getLogo());
        return view;
    }

    private Component renderString(final String value) {
        final Text textView = new Text(getContext());
        textView.setText(value);
        textView.setMarginBottom(10);
        textView.setMarginTop(10);
        textView.setPadding(20, 10, 10, 10);
        textView.setTextSize(TEXT_SIZE, Text.TextSizeType.VP);
        return textView;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    private static class CarNameUpdater implements Text.TextObserver {

        private Car carToUpdate;

        public CarNameUpdater(Car carToUpdate) {
            this.carToUpdate = carToUpdate;
        }

        @Override
        public void onTextUpdated(String s, int i, int i1, int i2) {
            carToUpdate.setName(s);
        }
    }

}
