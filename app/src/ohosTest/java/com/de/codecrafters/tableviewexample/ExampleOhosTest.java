/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.de.codecrafters.tableviewexample;

import com.de.codecrafters.tableviewexample.data.Car;
import com.de.codecrafters.tableviewexample.data.DataFactory;

import de.codecrafters.tableview.AttrSetString;
import de.codecrafters.tableview.util.LogUtil;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Random;

import static org.junit.Assert.assertEquals;

/**
 * 单元测试
 */
public class ExampleOhosTest {
    /**
     * 测试包名
     */
    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.de.codecrafters.tableviewexample", actualBundleName);
    }

    List<Car> carList;

    /**
     * 前置准备
     */
    @Before
    public void setUp() {
        carList = DataFactory.createCarList();
    }

    /**
     * 数据测试
     */
    @Test
    public void testGetCarList() {
        final List<Car> expecteds = DataFactory.createCarList();
        Assert.assertEquals(expecteds.size(), 16);
    }

    /**
     * 随机测试
     */
    @Test
    public void testRandomData() {
        final int randomCarIndex = Math.abs(new Random().nextInt() % carList.size());
        carList.get(randomCarIndex);
        Assert.assertEquals(carList.get(randomCarIndex) instanceof Car, true);
    }

    /**
     * 标题颜色测试
     */
    @Test
    public void testHeaderColorString() {
        Assert.assertEquals(AttrSetString.TABLEVIEW_HEADERCOLOR, "tableView_headerColor");
    }

    /**
     * 自定义值测试
     */
    @Test
    public void testHeaderElevationString() {
        Assert.assertEquals(AttrSetString.TABLEVIEW_HEADERELEVATION, "tableView_headerElevation");
    }

    /**
     * 列数测试
     */
    @Test
    public void testColumnCountString() {
        Assert.assertEquals(AttrSetString.TABLEVIEW_COLUMNCOUNT, "tableView_columnCount");
    }

    /**
     * 日志TAG测试
     */
    @Test
    public void testLogTAG() {
        Assert.assertEquals(LogUtil.TAG, "HMS_TAG");
    }
}