## 1.0.0
SortableTableView组件openharmony release版本

## 0.0.1-SNAPSHOT
SortableTableView组件openharmony第一个beta版本

#### 已实现功能

* 实现列表数据按每列数据进行升序/降序排序
* 可自定义列数
* 下拉刷新

#### api差异

* 无

#### 未实现功能

* Component的销毁保存数据和重绘恢复数据功能未实现
* 设置阴影效果未实现